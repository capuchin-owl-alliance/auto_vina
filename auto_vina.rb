#!/usr/bin/ruby

require 'optparse'

VERSION = '0.0.4.1'
DEDICATION = {
  to: 'Susana Restrepo',
  for: 'Any lossless representation of the reasons would overflow all known buffers'
}

def main!(options)

  receptor_dir = options[:receptor_dir]
  ligand_dir = options[:ligand_dir]
  output_dir = options[:output_dir]

  box_size = options[:box_size]
  exhaustiveness = options[:exhaustiveness]

  # Loop through receptors and
  Dir.foreach(receptor_dir) do |receptor_file|
    next unless receptor_file =~ /\.pdbqt$/ # skip if not pdbqt file

    location_file_name = receptor_file.sub(/pdbqt/, 'config')
    location_string = File.open([receptor_dir, location_file_name].join('/')).read
    location_hash = parse_location_file(location_string)

    x = location_hash['center_x']
    y = location_hash['center_y']
    z = location_hash['center_z']

    Dir.foreach(ligand_dir) do |ligand_file|
      next unless ligand_file =~ /\.pdbqt$/
      next if ligand_file =~ /_out.pdbqt$/

      sizex = sizey = sizez = box_size

      exec_str = "vina --receptor \"#{File.join(receptor_dir, receptor_file)}\" " +\
                 "--ligand \"#{File.join(ligand_dir, ligand_file)}\" " +\
                 "--center_x #{x} --center_y #{y} --center_z #{z} " +\
                 "--size_x #{sizex} --size_y #{sizey} --size_z #{sizez} " +\
                 "--exhaustiveness #{exhaustiveness} --out \"#{receptor_file}_#{ligand_file}_out.pdbqt\""

      Kernel.system exec_str
    end
  end

  Kernel.system "mkdir out"
  Kernel.system "mv #{ligand_dir}/*_out.pdbqt ./out"

  puts "The batch is complete"

end

def parse_location_file(location_string)
  # yoink from file, split by line
  lines = location_string.split("\n")

  # separate into clean key/value pairs
  parsed_lines = lines.map do |line|
    line.split("=").map { |str| str.strip }
  end

  # convert to hash
  lines_hash = Hash.new

  parsed_lines.each do |line|
    lines_hash[line.first] = line.last
  end

  return lines_hash
end

options = {
  box_size: 20,
  exhaustiveness: 20,
  receptor_dir: './receptors',
  ligand_dir: './ligands',
  output_dir: './output'
}

OptionParser.new do |opts|

  opts.banner = "Usage: auto_vina [--box BOX_SIZE] [--exhaust EXHAUSTIVENESS] [--receptors RECEPTOR_DIR] [--ligands LIGAND_DIR] [--outpuut OUTPUR_DIR]"

  opts.on "-b", "--box [BOX_SIZE]", "Size of the search box" do |box_size|
    options[:box_size] = box_size
  end

  opts.on "-e", "--exhaust [EXHAUSTIVENESS]",
    "Exhaustiveness parameter to pass to Vina" do |exhaustiveness|

    options[:exhaustiveness] = Exhaustiveness
  end

  opts.on "-r", "--receptors [RECEPTOR_DIR]",
    "Directory holding receptor PDB's" do |receptor_dir|

    options[:receptor_dir] = receptor_dir
  end

  opts.on "-l", "--ligands [LIGAND_DIR]",
    "Directory holding ligand PDB's" do |ligand_dir|

    options[:ligand_dir] = ligand_dir
  end

  opts.on "-o", "--output [OUTPUT_DIR]", "Directory for output" do |output_dir|
    options[:outpur_dir] = output_dir
  end

  opts.separator ""
  opts.separator "Common options:"

  opts.on_tail "-h", "--help", "Show this message" do
    puts opts
    exit
  end

  opts.on_tail "-v", "--version", "Show version" do
    puts "AutoVina v. #{VERSION}"
    exit
  end

  opts.on_tail "-d", "--dedication", "Show dedication" do
    puts "To: #{DEDICATION[:to]}"
    puts "For: #{DEDICATION[:for]}"
    exit
  end
end.parse!


main!(options)
