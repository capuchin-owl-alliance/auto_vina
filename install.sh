#!/bin/bash

mkdir auto_vina_tmp
cd auto_vina_tmp

git clone https://gitlab.com/capuchin-owl-alliance/auto_vina.git

cd auto_vina

chmod a+x ./auto_vina.rb
sudo cp ./auto_vina.rb /usr/bin/auto_vina

cd ../..

rm -rf ./auto_vina_tmp
